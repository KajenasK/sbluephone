﻿namespace SBluePhoneTests
{
    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
    using SBluePhone.MainBundle;
    [TestClass]
    public class SBlueTestOverall
    {
        [TestMethod]
        public void PresenterBeingInitialized()
        {
            // Arrange.
            var sut = new Controller();
            // Act.
            // Assert.
            Assert.AreNotEqual(null, sut, "Presenter was not initialized correctly.");
        }
    }
}
