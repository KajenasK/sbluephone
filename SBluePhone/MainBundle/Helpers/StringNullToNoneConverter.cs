﻿namespace SBluePhone.MainBundle.Helpers
{
    using System;
    using Windows.UI.Xaml.Data;
    public class StringNullToNoneConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if ((value as string) == null)
                return "Not bound.";

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
