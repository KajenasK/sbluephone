﻿namespace SBluePhone.MainBundle.Helpers
{
    using System;
    using Windows.UI.Xaml.Data;
    public class ToggleButtonContentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value as bool?).HasValue && !(value as bool?).Value ? "Start" : "Stop";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
