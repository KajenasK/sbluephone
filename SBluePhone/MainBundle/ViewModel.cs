﻿namespace SBluePhone.MainBundle
{
    using Components;
    using System.Collections.Generic;
    using System.Reactive.Subjects;
    using System.Windows.Input;
    using Windows.Networking.Proximity;

    class ViewModel : NotifyPropertyChangedFactory
    {
        public ICommand SecurityActionCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand UpdateDeviceList { get; set; }
        public ICommand ListBoxTappedCommand { get; set; }
        public ICommand UpdateSensorDataCommand { get; set; }
        public ICommand ResetSensorDataCommand { get; set; }
        public ICommand SensorScanCommand { get; set; }


        private bool? _isStartToggleEnabled;
        public bool? IsStartToggleEnabled
        {
            get { return _isStartToggleEnabled; }
            set
            {
                _isStartToggleEnabled = value;
                OnPropertyChanged();

                IsStartToggleEnabledObservable.OnNext(value);
            }
        }

        public ISubject<bool?> IsStartToggleEnabledObservable = new Subject<bool?>();


        private bool _isSensorAvailable;
        public bool IsSensorAvailable
        {
            get { return _isSensorAvailable; }
            set
            {
                _isSensorAvailable = value;
                OnPropertyChanged();

                IsSensorAvailableObservable.OnNext(value);
            }
        }

        public ISubject<bool> IsSensorAvailableObservable = new Subject<bool>();

        private bool _isStartButtonEnabled;
        public bool IsStartButtonEnabled
        {
            get { return _isStartButtonEnabled; }
            set
            {
                _isStartButtonEnabled = value;
                OnPropertyChanged();

                IsStartButtonEnabledObservable.OnNext(value);
            }
        }

        public ISubject<bool> IsStartButtonEnabledObservable = new Subject<bool>();


        private object _selectedDevice;

        public object SelectedDevice
        {
            get { return _selectedDevice; }
            set
            {
                _selectedDevice = value;
                OnPropertyChanged();

                SelectedDeviceObservable.OnNext(value);
            }
        }

        public ISubject<object> SelectedDeviceObservable = new Subject<object>();


        private List<PeerInformation> _deviceList;

        public List<PeerInformation> DeviceList
        {
            get { return _deviceList; }
            set
            {
                _deviceList = value;
                OnPropertyChanged();
            }
        }

        public ISubject<List<PeerInformation>> DeviceListObservable = new Subject<List<PeerInformation>>();


        private uint _secureStepsCount;
        public uint SecureStepsCount
        {
            get { return _secureStepsCount; }
            set
            {
                _secureStepsCount = value;
                OnPropertyChanged();

                SecureStepsCountObservable.OnNext(value);
            }
        }

        public ISubject<uint> SecureStepsCountObservable = new Subject<uint>();

        private uint _resetTime;

        public uint ResetTime
        {
            get { return _resetTime; }
            set
            {
                _resetTime = value;
                OnPropertyChanged();

                ResetTimeObservable.OnNext(value);
            }
        }

        public ISubject<uint> ResetTimeObservable = new Subject<uint>();

        private bool _requestEnabled = true;

        public bool RequestEnabled
        {
            get { return _requestEnabled; }
            set
            {
                _requestEnabled = value;
                OnPropertyChanged();

                RequestEnabledObservable.OnNext(value);
            }
        }

        public ISubject<bool> RequestEnabledObservable = new Subject<bool>();


        private string _boundToDeviceName;

        public string BoundToDeviceName
        {
            get { return _boundToDeviceName; }
            set
            {
                _boundToDeviceName = value;
                OnPropertyChanged();

                BoundToDeviceNameObservable.OnNext(value);
            }
        }

        public ISubject<string> BoundToDeviceNameObservable = new Subject<string>();



        private string _boundToDeviceMAC;

        public string BoundToDeviceMAC
        {
            get { return _boundToDeviceMAC; }
            set
            {
                _boundToDeviceMAC = value;
                OnPropertyChanged();

                BoundToDeviceMACObservable.OnNext(value);
            }
        }

        public ISubject<string> BoundToDeviceMACObservable = new Subject<string>();

    }
}
