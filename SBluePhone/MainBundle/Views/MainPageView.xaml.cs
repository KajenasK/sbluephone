﻿namespace SBluePhone.MainBundle.Views
{
    using Windows.UI.Xaml.Controls;
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPageView : Page
    {
        public MainPageView()
        {
            this.InitializeComponent();
        }
    }
}
