﻿namespace SBluePhone.MainBundle
{
    using Views;
    using Components;
    using DataAccess;
    using Windows.UI.Xaml;
    using System;
    using Windows.UI.Popups;
    using Windows.Storage;
    using Values;
    using Windows.Networking.Proximity;
    using Contracts;
    using Helpers;
    using System.Threading.Tasks;
    using System.Threading;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reactive.Subjects;
    using System.Reactive.Linq;
    public class Controller
    {
        private ViewModel _vM;
        private SensorModel _sensorData;
        private BluetoothCommunication _btData;
        private MainPageView _mainPage;
        private ApplicationDataContainer _localSettings;
        private ISubject<bool> _performSecurityActionObservable = new Subject<bool>();
        private CancellationTokenSource _cancellationTokenSource;
        private static uint _stepsTrack;
        private static uint _timeFrame;

        public Controller()
        {
            _sensorData = new SensorModel();
            _btData = new BluetoothCommunication();
            _vM = new ViewModel();
            _mainPage = new MainPageView();
            _mainPage.DataContext = _vM = new ViewModel();

            _localSettings = ApplicationData.Current.LocalSettings;

            Window.Current.Content = _mainPage;
            _cancellationTokenSource = new CancellationTokenSource();

            BuildUpController();
        }

        private void BuildUpController()
        {
            CommandBuilder();
            BuildAsyncParts();
            SetSensorViewValues();
            BuildSubscriptions();
            SetupViewValues();
        }

        private void SetupViewValues()
        {
            _vM.BoundToDeviceName = _localSettings.Values[MagicStrings.DEVICENAME] as string;
            _vM.BoundToDeviceMAC = _localSettings.Values[MagicStrings.DEVICEMAC] as string;

        }

        private void BuildSubscriptions()
        {
            _performSecurityActionObservable.Subscribe(async performSecurityAction => { if (performSecurityAction) await SendDataViaBT(MagicStrings.SECUREDEVICEMESSAGE); });

            _vM.BoundToDeviceNameObservable.Merge(_vM.BoundToDeviceMACObservable).Subscribe(value =>
            {
                _vM.RequestEnabled = value != null;
                _vM.IsStartButtonEnabled = _vM.IsSensorAvailable ? value != null : false;
            });

            _vM.IsSensorAvailableObservable.Subscribe(Values => _vM.IsStartButtonEnabled = _vM.IsSensorAvailable ? _vM.BoundToDeviceName != null && _vM.BoundToDeviceMAC != null : false);
        }

        public async void BuildAsyncParts()
        {
            await _sensorData.LocationTrackCapabilityCheck();
            _vM.IsSensorAvailable = await _sensorData.IsStepCountSupported();
        }

        private void SetSensorViewValues()
        {
            _vM.ResetTime = _localSettings.Values[MagicStrings.RESETTIME] == null ? FallbackValues.RESETTIMEDEFAULT : UInt32.Parse(_localSettings.Values[MagicStrings.RESETTIME]?.ToString());

            _vM.SecureStepsCount = _localSettings.Values[MagicStrings.STEPCOUNT] == null ? FallbackValues.STEPCOUNTDEFAULT : UInt32.Parse(_localSettings.Values[MagicStrings.STEPCOUNT]?.ToString());
        }

        private void CommandBuilder()
        {
            _vM.SecurityActionCommand = new ActionCommand(SecureComputerHandler);
            _vM.UpdateDeviceList = new ActionCommand(UpdateDeviceHandler);
            _vM.ListBoxTappedCommand = new ActionCommand(TappedHandler);
            _vM.UpdateSensorDataCommand = new ActionCommand(UpdateSensorDataHandler);
            _vM.ResetSensorDataCommand = new ActionCommand(ResetSensorDataHandler);
            _vM.SensorScanCommand = new ActionCommand(SensorActionHandler);
        }

        #region Handlers
        private void ResetSensorDataHandler()
        {
            _localSettings.Values[MagicStrings.STEPCOUNT] = FallbackValues.STEPCOUNTDEFAULT;
            _vM.SecureStepsCount = FallbackValues.STEPCOUNTDEFAULT;
            _localSettings.Values[MagicStrings.RESETTIME] = FallbackValues.RESETTIMEDEFAULT;
            _vM.ResetTime = FallbackValues.RESETTIMEDEFAULT;
        }

        private void UpdateSensorDataHandler()
        {
            _localSettings.Values[MagicStrings.RESETTIME] = _vM.ResetTime;
            _localSettings.Values[MagicStrings.STEPCOUNT] = _vM.SecureStepsCount;
        }

        private async void TappedHandler()
        {
            if (String.IsNullOrEmpty((_vM.SelectedDevice as PeerInformation)?.DisplayName))
                return;

            MessageDialog dlg = new MessageDialog($"Are you sure you want to bind your phone to {(_vM.SelectedDevice as PeerInformation)?.DisplayName}?", "Question");
            dlg.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(_ =>
            {
                _localSettings.Values[MagicStrings.DEVICENAME] = (_vM.SelectedDevice as PeerInformation)?.DisplayName;
                _localSettings.Values[MagicStrings.DEVICEMAC] = (_vM.SelectedDevice as PeerInformation)?.HostName.ToString();
            })));
            dlg.Commands.Add(new UICommand("No"));
            await dlg.ShowAsync();

            _vM.BoundToDeviceName = _localSettings.Values[MagicStrings.DEVICENAME] as string;
            _vM.BoundToDeviceMAC = _localSettings.Values[MagicStrings.DEVICEMAC] as string;

        }

        private async void UpdateDeviceHandler()
        {
            _vM.DeviceList = await _btData.GetPairedDeviceListAsync();
            if (_vM.DeviceList == null)
            {
                MessageDialog dlg = new MessageDialog("Issue while connecting to other device. Try again.", "Information");
                dlg.Commands.Add(new UICommand("Ok"));
                await dlg.ShowAsync();
            }
        }

        private async void SecureComputerHandler()
        {
            _vM.RequestEnabled = false;
            if (!await SendDataViaBT(MagicStrings.SECUREDEVICEMESSAGE))
            {
                MessageDialog dlg = new MessageDialog("Error occurred with Bluetooth module. Please try restarting Bluetooth off and on and reconnecting with your machine.", "Information");
                dlg.Commands.Add(new UICommand("Ok"));
                await dlg.ShowAsync();
            }
            _vM.RequestEnabled = true;
        }

        private void SensorActionHandler()
        {
            if (_vM.IsStartToggleEnabled.HasValue && _vM.IsStartToggleEnabled.Value)
            {
                _cancellationTokenSource = new CancellationTokenSource();
                var cancelationToken = _cancellationTokenSource.Token;

                _timeFrame = UInt32.Parse(_localSettings.Values[MagicStrings.RESETTIME]?.ToString());
                _stepsTrack = UInt32.Parse(_localSettings.Values[MagicStrings.STEPCOUNT]?.ToString());

                var request = new StepLockingRequest() { Token = cancelationToken };

                Task sensorDataLocking = new Task(SensorDataHandler, request);
                sensorDataLocking.Start();
            }
            else
            {
                _cancellationTokenSource?.Cancel();
            }
        }

        public async void SensorDataHandler(object request)
        {
            uint stepsCount = 0;
            uint time = 0;

            uint firstValue = 0;

            var ct = (request as StepLockingRequest).Token;
            Queue<uint> monitorValues = new Queue<uint>();
            while (true)
            {
                if (ct.IsCancellationRequested)
                    return;

                stepsCount = 0;
                stepsCount += (uint)await (_sensorData as IModelProxy)?.GetData(StepType.Walking);
                stepsCount += (uint)await (_sensorData as IModelProxy)?.GetData(StepType.Running);

                monitorValues.Enqueue(stepsCount);

                if (monitorValues.Count > _timeFrame)
                    monitorValues.Dequeue();

                firstValue = monitorValues.FirstOrDefault();

                if ((firstValue + _stepsTrack) < stepsCount)
                    _performSecurityActionObservable.OnNext(true);

                await Task.Delay(1000);
                time++;
            }

        }
        #endregion

        private async Task<bool> SendDataViaBT(string message)
        {
            return await _btData.SendData(new BluetoothMessageRequest() { Message = message, DeviceName = _localSettings.Values[MagicStrings.DEVICENAME] as string });
        }
    }
}
