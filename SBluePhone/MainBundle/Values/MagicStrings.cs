﻿namespace SBluePhone.MainBundle.Values
{
    public class MagicStrings
    {
        public const string DEVICENAME = "DeviceNameKey";
        public const string DEVICEMAC = "DeviceMacKey";
        public const string STEPCOUNT = "StepCountKey";
        public const string RESETTIME = "ResetTimeKey";

        // BT Communcation strings.
        public const string SECUREDEVICEMESSAGE = "Perform security action <EOF>";
    }
}
