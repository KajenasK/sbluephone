﻿namespace SBluePhone.MainBundle.Values
{
    public class FallbackValues
    {
        public const uint STEPCOUNTDEFAULT = 10;
        public const uint RESETTIMEDEFAULT = 20;
    }
}
