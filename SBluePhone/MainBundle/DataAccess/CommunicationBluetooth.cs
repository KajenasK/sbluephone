﻿namespace SBluePhone.MainBundle.DataAccess
{
    using System;
    using System.Diagnostics;
    using Windows.Networking.Proximity;
    using System.Diagnostics.Contracts;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Windows.Networking.Sockets;
    using System.Runtime.InteropServices.WindowsRuntime;
    using Windows.Devices.Bluetooth.Rfcomm;
    using Windows.Devices.Enumeration;
    using Contracts;
    class BluetoothCommunication : IModelProxy, IBluetooth
    {
        [Pure]
        public async Task<List<PeerInformation>> GetPairedDeviceListAsync()
        {
            List<PeerInformation> localDeviceList = new List<PeerInformation>();

            try
            {
                PeerFinder.AlternateIdentities["Bluetooth:PAIRED"] = "";
                foreach (var device in await PeerFinder.FindAllPeersAsync())
                {
                    localDeviceList.Add(device);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }

            return localDeviceList;
        }

        public Task<object> GetData(object request)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> SendData(object request)
        {
            try
            {
                foreach (var di in await DeviceInformation.FindAllAsync(RfcommDeviceService.GetDeviceSelector(RfcommServiceId.SerialPort)))
                {
                    if (di.Name.Contains((request as BluetoothMessageRequest)?.DeviceName))
                    {
                        var device = await RfcommDeviceService.FromIdAsync(di.Id);

                        using (StreamSocket streamSocket = new StreamSocket())
                        {
                            await streamSocket.ConnectAsync(device.ConnectionHostName, device.ConnectionServiceName, device.ProtectionLevel);

                            var sendMessage = streamSocket.OutputStream;
                            var result = await sendMessage.WriteAsync(System.Text.Encoding.UTF8.GetBytes((request as BluetoothMessageRequest)?.Message)?.AsBuffer());

                            if (result == (request as BluetoothMessageRequest)?.Message?.Length)
                                return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return false;
        }
    }
}
