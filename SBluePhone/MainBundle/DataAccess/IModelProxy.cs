﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SBluePhone.MainBundle.DataAccess
{
    interface IModelProxy
    {
        Task<bool> SendData(object request);
        Task<object> GetData(object request);
    }
}
