﻿namespace SBluePhone.MainBundle.DataAccess
{
    using Lumia.Sense;
    using System;
    using System.Threading.Tasks;
    using Windows.UI.Popups;
    using SBluePhone.MainBundle.Helpers;

    class SensorModel : IModelProxy
    {
        StepCounter _stepCounter;

        public SensorModel()
        {
        }

        public async Task<bool> LocationTrackCapabilityCheck()
        {
            if (!await StepCounter.IsSupportedAsync())
            {
                MessageDialog dlg = new MessageDialog("Your phone does not support step counting.", "Information");
                dlg.Commands.Add(new UICommand("Ok"));
                await dlg.ShowAsync();

                return false;
            }

            MotionDataSettings settings = await SenseHelper.GetSettingsAsync();

            if (settings.Version < 2)
            {
                if (!settings.LocationEnabled)
                {
                    MessageDialog dlg = new MessageDialog("In order to count steps you need to enable location in system settings. Do you want to open settings now? ", "Information");

                    dlg.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(
                        async (cmd) => await SenseHelper.LaunchLocationSettingsAsync())));
                    dlg.Commands.Add(new UICommand("No"));
                    await dlg.ShowAsync();
                }
            }
            return settings.LocationEnabled;
        }


        public async Task<bool> IsStepCountSupported()
        {
            if (!await StepCounter.IsSupportedAsync())
                return false;

            MotionDataSettings settings = await SenseHelper.GetSettingsAsync();
            return settings.LocationEnabled;
        }


        private async Task<bool> CallSensorcoreApiAsync(Func<Task> action)
        {
            Exception failure = null;

            try
            {
                await action();
            }
            catch (Exception e)
            {
                failure = e;
            }

            if (failure != null)
            {
                MessageDialog dialog;

                switch (SenseHelper.GetSenseError(failure.HResult))
                {
                    case SenseError.LocationDisabled:
                        dialog = new MessageDialog("Location has been disabled. Do you want to open Location settings now? ", "Information");


                        dialog.Commands.Add(new UICommand("Yes", async cmd => await
                      SenseHelper.LaunchLocationSettingsAsync()));

                        dialog.Commands.Add(new UICommand("No"));
                        await dialog.ShowAsync();
                        new System.Threading.ManualResetEvent(false).WaitOne(500);
                        return false;

                    case SenseError.SenseDisabled:
                        {
                            var settings = await SenseHelper.GetSettingsAsync();

                            if (settings.Version < 2)
                                dialog = new MessageDialog("Motion data has been disabled. Do you want to open motion data settings now? ", "Information");
                            else
                                dialog = new MessageDialog("Places visited has been disabled. Do you want to open motion data settings now? ", "Information");

                            dialog.Commands.Add(new UICommand("Yes", new
                                    UICommandInvokedHandler(async (cmd) => await
                                   SenseHelper.LaunchSenseSettingsAsync())));

                            dialog.Commands.Add(new UICommand("No"));
                            await dialog.ShowAsync();
                            new System.Threading.ManualResetEvent(false).WaitOne(500);
                            return false;
                        }

                    default:
                        dialog = new MessageDialog("Failure: " + SenseHelper.GetSenseError(
                                failure.HResult), "");
                        await dialog.ShowAsync();
                        return false;
                }
            }
            return true;
        }

        public Task<bool> SendData(object request)
        {
            throw new NotImplementedException();
        }

        public async Task<object> GetData(object request)
        {
            if (request is StepType)
            {
                switch ((StepType)request)
                {
                    case StepType.Running:
                        return await RunningStepCount();
                    case StepType.Walking:
                        return await WalkingStepCount();
                    default:
                        return null;
                }
            }
            return null;
        }
        private async Task<uint> WalkingStepCount()
        {
            StepCounterReading currentSensorData = await Factory();
            return currentSensorData.WalkingStepCount;
        }

        private async Task<uint> RunningStepCount()
        {
            StepCounterReading currentSensorData = await Factory();
            return currentSensorData.RunningStepCount;
        }

        private async Task<StepCounterReading> Factory()
        {
            await CallSensorcoreApiAsync(async () => _stepCounter = await StepCounter.GetDefaultAsync());
            return await _stepCounter.GetCurrentReadingAsync();
        }
    }
}
