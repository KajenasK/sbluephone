﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Networking.Proximity;

namespace SBluePhone.MainBundle.DataAccess
{
    interface IBluetooth
    {
        Task<List<PeerInformation>> GetPairedDeviceListAsync();
    }
}
