﻿using System.Threading;

namespace SBluePhone.Contracts
{
    public class StepLockingRequest
    {
        public uint Time { get; set; }
        public uint Steps { get; set; }
        public CancellationToken Token { get; set; }
    }
}
