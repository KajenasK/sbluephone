﻿namespace SBluePhone.Contracts
{
    class BluetoothMessageRequest
    {
        public string Message { get; set; }
        public string DeviceName { get; set; }
    }
}
