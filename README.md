# README #

Bluetooth power security app. Performs security measure on Windows machine which runs desktop version of this application.
You would require a desktop application to cooperate with Windows Phone 8.1 app. It will run regardless but will not function. You require a windows machine with bluetooth module.

### What is this repository for? ###

* To store source of my Bachelor degree task.
* 1.0

### Roadmap ###

* Make BT communication with other Windows machine via sockets.
* Create an algorithm to lock Windows machine depending on user specified parameters.
* Fluent UX.
* Make Desktop App available on GIT.

### How do I get set up? ###

* Run the code in VS, deploy to WP 8.1 device.

### Contribution guidelines ###

* Tests are currently not present.
* Using MVVMC and RX patterns for app's architecture.

### Who do I talk to? ###

* karolis.kajenas1@gmail.com